import org.json.*;

import java.io.FileInputStream;

public class JSONParserJava
{
  public static void main (String[] args)
  {
    System.out.println("JSONParser - Java");

    // Open the input file.
    try (FileInputStream inputStream = new FileInputStream("input.json"))
    {
      // Parse the JSON input into an JSON object and initialize the project model with it.
      JSONObject json = new JSONObject(new JSONTokener(inputStream));
      Project project = new Project(json);

      // Output the names of the project and the project category.
      System.out.println("\nThe name of the container project is \"" + project.name + "\".");
      System.out.println("The name of the container project category is \"" + project.projectCategory.name + "\".");

      // Output the names of all issue types.
      System.out.println("\nThe names of the container project issuetypes are:");
        for (IssueType issueType : project.issueTypes)
          System.out.print("\"" + issueType.name + "\" ");

      // Output the names of all issue types that have a subtask property that is true.
      System.out.println("\n\nThe names of the container project issue types, where subtask is true:");
        for (IssueType issueType : project.issueTypes)
          if (issueType.subtask == true)
            System.out.print("\"" + issueType.name + "\" ");
     }
 		 catch (Exception ex)
 		 {
      // In an actual project I would - of course - handle exceptions properly.
      // But for reasons of simplicity and to keep the code more concise
      // I will not add more elaborated exception handling.
      System.out.println(ex);
 		 }

  }
}
