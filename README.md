# jsonparser-java

## What is this?
This is a solution for the task described here: https://mygit.th-deg.de/tlang/jsonparser/-/tree/master



## Why Java

The problem described in the task can also be solved with Java.  Default installations of the JDK do not come with good built-in ways to parse JSON - so I resorted to the **JSON in Java** package (https://github.com/stleary/JSON-java). With that package, parsing JSON is quite simple and straightforward.

The other reason why I chose to come up with an alternative solution for the problem described in the task is that I also wanted to provide a solution that is implemented in a programming language that is often considered to be more mainstream than Ruby.



## Design decisions regarding the object model

I used ArrayLists for the ***avatarUrls*** and ***roles*** properties of the input JSON. Since I don't want to be too redundant, you can find more about the reasons behind this in the README of my Ruby implementation: https://bitbucket.org/mheyd/jsonparser-ruby



## How to run it

* Make sure the Java SDK 8 is installed.
* Clone or download and unzip the project.
* Go to the directory where the downloaded/cloned project sits on your disk.
* You will need the **JSON in Java** (org.json) package from  https://github.com/stleary/JSON-java. When you don't have it already in your library, you have to download and unzip it in the folder that contains this project.
```
wget https://repo1.maven.org/maven2/org/json/json/20200518/json-20200518.jar -O json.jar
jar xf json.jar
```
* This should provide you with an **org/json** folder in the current directory that contains all required classes.  You can also download and unzip the packet manually, of course, or install the package in your library path.

* Compile the program (and all dependencies):
   `javac JSONParserJava.java`

* Run the program:
   `java JSONParserJava`



## Program output

This is what you should get when you run the program:

```
JSONParser - Java

The name of the container project is "123-Musterprojekt".
The name of the container project category is "Softwareprojekt".

The names of the container project issuetypes are:
"Öffentliche FuE-Projekt" "Bearbeitung DM" "Vertragsentwurf" "Vertragspruefung" "Sub-Task"

The names of the container project issue types, where subtask is true:
"Bearbeitung DM" "Vertragsentwurf" "Sub-Task"
```



## Error handling

I did not add any real error handling. In an application, that runs in a production environment, I would, of course, make sure that all sorts of problems are covered. For reasons of simplicity I did not venture into making this program "idiot safe". But I am aware that real inputs need to be checked and that there need to be mechanisms in place to react, when improper JSON is sent to a real application.



## Commenting

I did not really use tons of comments - but since the problem at hand is quite simple to solve and doesn't require any real advanced programming techniques, I did not use too many comments. I usually comment my code very well - in this case it's a bit hard, because very simple loops and assignments don't require a lot of comments that are really helpful.



## Contact

If you have any further questions I'm happy to answer them: mh@mheyd.com.
