public class AvatarUrl
{

  public String format;
  public String url;

  public AvatarUrl(String format, String url)
  {
    this.format = format;
    this.url = url;
  }

}
