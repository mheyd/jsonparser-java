import org.json.*;

import java.util.ArrayList;

public class Lead
{

  public String self;
  public String key;
  public String name;
  public ArrayList<AvatarUrl> avatarUrls;
  public String displayName;
  public Boolean active;

  public Lead(JSONObject json)
  {
    try
    {
      self = json.getString("self");
      key = json.getString("key");
      name = json.getString("name");
      displayName = json.getString("displayName");
      active = json.getBoolean("active");
      avatarUrls = Helper.InitializeAvatarUrls(json.getJSONObject("avatarUrls"));
    }
    catch (Exception ex)
    {
      System.out.println(ex);
    }
  }

}
