import org.json.*;

import java.util.Iterator;
import java.util.ArrayList;

public class Helper
{

  public static ArrayList<AvatarUrl> InitializeAvatarUrls(JSONObject json)
  {
    ArrayList<AvatarUrl> avatarUrls = new ArrayList<AvatarUrl>();

    // Go through all keys in the JSON object.
    Iterator<String> keys = json.keys();
    while(keys.hasNext()) {
        String key = keys.next();
        // Initialize a new AvatarUrl instance from the key/value combo.
        AvatarUrl avatarUrl = new AvatarUrl(key, json.getString(key));
        avatarUrls.add(avatarUrl);
    }

    return avatarUrls;
  }

  public static ArrayList<Component> InitializeComponents(JSONArray jsonArray)
  {
    ArrayList<Component> components = new ArrayList<Component>();

    // Go through all elements in the JSON array.
    for (int i = 0; i < jsonArray.length(); i++)
    {
      Component component = new Component(jsonArray.getJSONObject(i));
      components.add(component);
    }

    return components;
  }

  public static ArrayList<IssueType> InitializeIssueTypes(JSONArray jsonArray)
  {
    ArrayList<IssueType> issueTypes = new ArrayList<IssueType>();

    // Go through all elements in the JSON array.
    for (int i = 0; i < jsonArray.length(); i++)
    {
      IssueType issueType = new IssueType(jsonArray.getJSONObject(i));
      issueTypes.add(issueType);
    }

    return issueTypes;
  }

  public static ArrayList<Version> InitializeVersions(JSONArray jsonArray)
  {
    ArrayList<Version> versions = new ArrayList<Version>();

    // Go through all elements in the JSON array.
    for (int i = 0; i < jsonArray.length(); i++)
    {
      Version version = new Version(jsonArray.getJSONObject(i));
      versions.add(version);
    }

    return versions;
  }

  public static ArrayList<Role> InitializeRoles(JSONObject json)
  {
    ArrayList<Role> roles = new ArrayList<Role>();

    // Go through all keys in the JSON object.
    Iterator<String> keys = json.keys();
    while(keys.hasNext()) {
        String key = keys.next();
        // Initialize a new Role instance from the key/value combo.
        Role role = new Role(key, json.getString(key));
        roles.add(role);
    }

    return roles;
  }

}
