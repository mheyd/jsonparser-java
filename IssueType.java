import org.json.*;

import java.util.OptionalInt;

public class IssueType
{

  public String self;
  public String id;
  public String description;
  public String iconUrl;
  public String name;
  public Boolean subtask;
  public OptionalInt avatarId;

  public IssueType(JSONObject json)
  {
    try
    {
      self = json.getString("self");
      id = json.getString("id");
      description = json.getString("description");
      iconUrl = json.getString("iconUrl");
      name = json.getString("name");
      subtask = json.getBoolean("subtask");
      avatarId = json.has("avatarId")
        ? OptionalInt.of(json.getInt("avatarId")) : OptionalInt.empty();
    }
    catch (Exception ex)
    {
      System.out.println(ex);
    }
  }

}
