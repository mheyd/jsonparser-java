import org.json.*;

public class ProjectCategory
{

  public String self;
  public String id;
  public String name;
  public String description;

  public ProjectCategory(JSONObject json)
  {
    try
    {
      self = json.getString("self");
      id = json.getString("id");
      name = json.getString("name");
      description = json.getString("description");
    }
    catch (Exception ex)
    {
      System.out.println(ex);
    }
  }


}
