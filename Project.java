import org.json.*;

import java.util.Iterator;
import java.util.ArrayList;

public class Project
{

  public String expand;
  public String self;
  public String id;
  public String key;
  public String description;
  public Lead lead;
  public ArrayList<Component> components;
  public ArrayList<IssueType> issueTypes;
  public String assigneeType;
  public ArrayList<Version> versions;
  public String name;
  public ArrayList<Role> roles;
  public ArrayList<AvatarUrl> avatarUrls;
  public ProjectCategory projectCategory;
  public String projectTypeKey;
  public Boolean archived;

  public Project(JSONObject json)
  {
    try
    {
      expand = json.getString("expand");
      self = json.getString("self");
      id = json.getString("id");
      key = json.getString("key");
      description = json.getString("description");
      lead = new Lead(json.getJSONObject("lead"));
      projectTypeKey = json.getString("projectTypeKey");
      archived = json.getBoolean("archived");
      components = Helper.InitializeComponents(json.getJSONArray("components"));
      issueTypes = Helper.InitializeIssueTypes(json.getJSONArray("issueTypes"));
      assigneeType = json.getString("assigneeType");
      versions = Helper.InitializeVersions(json.getJSONArray("versions"));
      name = json.getString("name");
      roles = Helper.InitializeRoles(json.getJSONObject("roles"));
      avatarUrls = Helper.InitializeAvatarUrls(json.getJSONObject("avatarUrls"));
      projectCategory = new ProjectCategory(json.getJSONObject("projectCategory"));
      projectTypeKey = json.getString("projectTypeKey");
      archived = json.getBoolean("archived");
    }
    catch (Exception ex)
    {
      System.out.println(ex);
    }
  }

}
